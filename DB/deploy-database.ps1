﻿<#
.SYNOPSIS 
Создать подключение к серверу БД.

.DESCRIPTION
Создать подключение к серверу БД.

.PARAMETER server
Имя сервера.

.OUTPUTS
Объект Microsoft.SqlServer.Management.Smo.Server
#>
function DatabaseServer_Connect([string] $server)
{
    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null
    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.BatchParser") | Out-Null
    $serverConnection = new-object Microsoft.SqlServer.Management.Common.ServerConnection
    $serverConnection.ServerInstance = $server

    # Подключаемся к серверу БД.
    $srv = New-Object Microsoft.SqlServer.Management.Smo.Server($serverConnection) -ErrorAction Stop
    $srv.ConnectionContext.StatementTimeout = 0
    $srv.ConnectionContext.NonPooledConnection = $true
	
    return $srv    
}

<#
.SYNOPSIS 
Удалить БД.

.DESCRIPTION
Удалить БД.

.PARAMETER databaseServerConfig
Раздел DatabaseServer.
#>
function Databases_Delete([Microsoft.SqlServer.Management.Smo.Server] $server, [System.Xml.XmlElement] $databaseServerConfig)
{
    foreach($db in $databaseServerConfig.Database)
    {
        Write-Host "DB $($db.Name) delete..." -NoNewline
        if (!$server.Databases.Contains($db.Name))
        {
            Write-Host "skipped" -ForegroundColor Green
        }
        else
        {
            $server.KillAllprocesses($db.Name)
            $server.Databases[$db.Name].Drop()
            Write-Host "done" -ForegroundColor Green
        }
    }
}

<#
.SYNOPSIS 
Создать БД.

.DESCRIPTION
Создать БД.

.PARAMETER server
Коннект к серверу БД.

.PARAMETER databaseServerConfig
Раздел DatabaseServer.

.PARAMETER extractedPackagePath
Путь к распакованному пакету.
#>
function Databases_Create([Microsoft.SqlServer.Management.Smo.Server] $server, [System.Xml.XmlElement] $databaseServerConfig, [string] $extractedPackagePath)
{	
    foreach($db in $databaseServerConfig.Database)
    {
        Write-Host "DB $($db.Name) check status..." -NoNewline
        if (!$server.Databases.Contains($db.Name))
        {
			# Создаем БД, если требуется
			$newDatabase = New-Object Microsoft.SqlServer.Management.Smo.Database($server, $db.Name)
			$newDatabase.Collation = "Cyrillic_General_CI_AS"
			$newDatabase.Create();
			Write-Host "created" -ForegroundColor Yellow
			
			$initializeScript = [System.IO.Path]::combine($extractedPackagePath, "DB", "Initialize.sql")
			Database_ExecuteScripts $newDatabase $initializeScript
			$onCreateScript = $databaseServerConfig.OnCreate
        }
        else
        {
            Write-Host "exist" -ForegroundColor Green
        }
    }
}

<#
.SYNOPSIS 
Обновить БД.

.DESCRIPTION
Обновить (применить скрипты миграции) к БД.

.PARAMETER server
Коннект к серверу БД.

.PARAMETER databaseServerConfig
Раздел DatabaseServer.

.PARAMETER countScriptsToUpdate
Количество скриптов для обновления.

.PARAMETER extractedPackagePath
Путь к распакованному пакету.
#>
function Databases_Update([Microsoft.SqlServer.Management.Smo.Server] $server, [System.Xml.XmlElement] $databaseServerConfig, [hashtable] $countScriptsToUpdate, [string] $extractedPackagePath)
{	
    foreach($db in $databaseServerConfig.Database)
    {
        $database = $server.Databases[$db.Name]
		
        # Если нечего обновлять, пропускаем БД.
        if ($countScriptsToUpdate -and $countScriptsToUpdate[$database.Name] -eq 0)
        {
            continue
        }

		# Получаем название скриптов, которые необходимо выполнить
        $scripts = GetScripts $extractedPackagePath
        $scripts = $scripts | sort
		Write-Host "DB $($db.Name) updating"
		
		# Выполняем обновление БД
        foreach($script in $scripts)
        {
			Database_ExecuteScripts $database $script.FullName
        }
    }
}

<#
.SYNOPSIS 
Выполнялся ли скрипт обновления/миграции БД.

.DESCRIPTION
Выполнялся ли скрипт обновления/миграции БД.

.PARAMETER db
База данных.

.PARAMETER filename
Имя файла с скриптом обновления.

.OUTPUTS
Выполнялось ли обновление ($TRUE/$FALSE).
#>
function IsExecuted([Microsoft.SqlServer.Management.Smo.Database] $db, [string] $filename)
{
    $countScript = "SELECT count(*) FROM [Config_Script] WHERE [ID] ='$($filename)'"
	
    # Проверяем, выполнялся ли скрипт обновления
    if ($db.ExecuteWithResults($countScript).Tables[0].Rows[0].ItemArray[0] -eq 1)
    {
        return $TRUE;
    }

    return $FALSE;
}

<#
.SYNOPSIS
Выполнить скрипт обновления/миграции БД.

.DESCRIPTION
Выполнить скрипт обновления/миграции БД.

.PARAMETER db
БД.

.PARAMETER filePath
Путь к скрипту обновления/миграции БД.
#>
function Database_ExecuteScripts([Microsoft.SqlServer.Management.Smo.Database] $db, [string] $filePath)
{	
    try
    {
        $filename = [io.path]::GetFileNameWithoutExtension($filePath)
        Write-Host "`t$filename..." -NoNewline

        # Выполняем первоначальное создание (инициализацию) БД.
        if ($filename -ne "Initialize")
        {
            if ((IsExecuted $db $filename) -eq $TRUE)
            {
                Write-Host "Skipped" -ForegroundColor DarkYellow
                return
            }
        }
		
        # Считываем скрипт миграции и выполняем
        $content = [System.IO.File]::ReadAllText($filePath)
        $db.ExecuteNonQuery($content)
        Write-Host "Done..." -NoNewline -ForegroundColor Green
		
        $dateTimeNow = [DateTime]::Now.ToString([CultureInfo]::InvariantCulture)
		$insertScript = "INSERT INTO [Config_Script] ([ID], [Description], [ExecutionDate]) VALUES('$($filename)','','$dateTimeNow')"
        $db.ExecuteNonQuery($insertScript)
        Write-Host "Inserted" -ForegroundColor Green
    }
    catch
    {
        Write-Error $_.Exception | Format-list -force
        throw
    }
}

<#
.SYNOPSIS
Получить список скриптов обновления БД.

.DESCRIPTION
Получить список скриптов обновления БД.

.PARAMETER extractedPackagePath
Путь к распакованному пакету.

.OUTPUT
Список скриптов обновления БД.
#>
function GetScripts([string]$extractedPackagePath, [string] $subPath="Update")
{
    $scriptsPath = [System.IO.Path]::combine($extractedPackagePath, "DB", $subPath)
    Get-ChildItem -File -Path $scriptsPath -Filter *.sql -ErrorAction Stop
}