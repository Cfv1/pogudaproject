CREATE TABLE [dbo].[Service](
	[ID] [int] IDENTITY(1,1),
	[Name] [varchar](255) NOT NULL,
	[Content] [varchar](255) NOT NULL,
	[PicturePath] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
INSERT INTO [dbo].[Service] ([Name],[Content],[PicturePath])
VALUES ('Google', 'Гугл это круто', 'https://downloader.disk.yandex.ru/preview/77a64cca381f29c4b368714bcc2f6b32a4bec625bbbe7b464cb6bf531b937f1f/5bf9657e/2YJ-eRNvLL2QbVQh1EWbe5XHNLC7qocXwo4FvvCv1lA2HlhShO_PtpyrdqJAsi8wTz2E5vRDfoNSXbFIjTkzOg%3D%3D?uid=0&filename=google.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=2048x2048')
GO
INSERT INTO [dbo].[Service] ([Name],[Content],[PicturePath])
VALUES ('Yandex', 'Яндекс это круто', 'https://downloader.disk.yandex.ru/preview/eca64fabdffaf1067370986eeb8cd06be01a64aa57765ee4a5996e1625da5b25/5bf96585/2YJ-eRNvLL2QbVQh1EWbezxEhhZlZ6qb90daxlXoJ2CD88nZMKGFuwbYUCl4CKFLjNhymsYt7fjlpxU7Ns88IQ%3D%3D?uid=0&filename=yandex.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=2048x2048')
GO
INSERT INTO [dbo].[Service] ([Name],[Content],[PicturePath])
VALUES ('Youtube', 'Ютуб это круто', 'https://downloader.disk.yandex.ru/preview/2e06e2c9311ed4ca1efa683a4839ce844ed2671fcaa040b9eec708baff33abf2/5bf96597/2YJ-eRNvLL2QbVQh1EWbeyvjvfGjZh88858xm_v9tGCrPQP14N0nmp9jmdr2K0gLeb8kIi-ORE_S6vBU3-n0eQ%3D%3D?uid=0&filename=youtube.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&size=2048x2048')
GO