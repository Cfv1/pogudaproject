CREATE TABLE [dbo].[Comment](
	[ID] [int] IDENTITY(1,1),
	[CommentatorName] [varchar](255) NOT NULL,
	[Commentary] [varchar](255) NOT NULL,
	[ServiceId] [int],
	FOREIGN KEY (ServiceId) REFERENCES Service(ID),
	CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED ([ID] ASC))
 
GO