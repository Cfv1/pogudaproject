Param
(
    [ValidateSet('db-delete','db-migrate','db-delete-migrate')]
	[string] $command = "db-delete-migrate",
	[string] $sqlserver = "(localdb)\MSSQLLocalDB",
	[string] $dbname = "Main",
	[string] $branch = "master"
)

function Get_Config()
{
    [xml]$databaseConfigXml = "<Databases></Databases>"
	$databaseElement = $databaseConfigXml.CreateElement("Database")
	$databaseElement.SetAttribute("Name", "$branch" + "_" + $dbname) | Out-Null
	$databaseConfigXml.SelectSingleNode("Databases").AppendChild($databaseElement) | Out-Null
    return $databaseConfigXml
}

function Db-Migrate()
{	
    . ..\DB\deploy-database.ps1
    $DatabaseServer = DatabaseServer_Connect -server $sqlserver
    $databaseConfigXml = Get_Config

    Databases_Create -server $DatabaseServer -databaseServerConfig $databaseConfigXml.Databases -extractedPackagePath "$(get-location)\.."
    Databases_Update -server $DatabaseServer -databaseServerConfig $databaseConfigXml.Databases -extractedPackagePath "$(get-location)\.."
}

function Db-Delete()
{
    . ..\DB\deploy-database.ps1
    $DatabaseServer = DatabaseServer_Connect -server $sqlserver -login $login -password $password
    $databaseConfigXml = Get_Config

    Databases_Delete -server $DatabaseServer -databaseServerConfig $databaseConfigXml.Databases
}

if ($branch -eq "this")
{
    $branch  = $(git rev-parse --abbrev-ref HEAD)
}

Write-Output "Sql server: $sqlserver"
Write-Output "Branch: $branch"
$dbpath = "..\DB\"

switch($command)
{
    "db-migrate" 
    {
        Db-Migrate
    }

    "db-delete" 
    {
        Db-Delete
    }

    "db-delete-migrate" 
    {
        Db-Delete
        Db-Migrate
    }
}