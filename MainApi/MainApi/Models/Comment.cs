namespace MainApi.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Comment")]
    public partial class Comment
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string CommentatorName { get; set; }

        [Required]
        [StringLength(255)]
        public string Commentary { get; set; }

        public int? ServiceId { get; set; }
        public virtual Service Service { get; set; }
    }
}
