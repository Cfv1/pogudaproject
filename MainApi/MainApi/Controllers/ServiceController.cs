﻿namespace MainApi.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Models;
    using Repositories;

    [MyCorsPolicy]
    [AllowAnonymous]
    [RoutePrefix("api/Service")]
    public class ServiceController : ApiController
    {
        private readonly ServiceRepository serviceRepository;

        public ServiceController(ServiceRepository serviceRepository)
        {
            this.serviceRepository = serviceRepository;
        }

        [Route("GetAll")]
        public async Task<IEnumerable<Service>> GetAll()
        {
            return await serviceRepository.GetAll();
        }

        [Route("Get")]
        public async Task<Service> Get(int id)
        {
            return await serviceRepository.Get(id);
        }

        [HttpGet]
        [Route("{id}/comments")]
        public async Task<IEnumerable<Comment>> GetAll(int id)
        {
            return await serviceRepository.GetCommentsById(id);
        }
    }
}