﻿namespace MainApi.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Models;
    using Repositories;

    [MyCorsPolicy]
    [AllowAnonymous]
    [RoutePrefix("api/Comment")]
    public class CommentController : ApiController
    {
        private readonly CommentRepository commentRepository;

        public CommentController(CommentRepository commentRepository)
        {
            this.commentRepository = commentRepository;
        }
    }
}