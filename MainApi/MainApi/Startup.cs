﻿using Microsoft.Owin; [assembly: OwinStartup(typeof(MainApi.Startup))]

namespace MainApi
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
