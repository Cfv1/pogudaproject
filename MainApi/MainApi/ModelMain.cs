
namespace MainApi
{
    using System.Data.Entity;
    using Models;
    
    public partial class ModelMain : DbContext
    {
        public ModelMain() : base("name=ModelMain")
        {
        }

        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Service> Service { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .Property(e => e.CommentatorName)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .Property(e => e.Commentary)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.PicturePath)
                .IsUnicode(false);
        }
    }
}
