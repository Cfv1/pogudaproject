﻿namespace MainApi.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Data.Entity;
    using Models;

    /// <summary>Интерфейс репозитория комментария. </summary>
    public interface ICommentRepository
    {
    }

    /// <summary>Репозиторий комментариев. </summary>
    public class CommentRepository : ICommentRepository
    {
        ModelMain db = new ModelMain();
    }
}