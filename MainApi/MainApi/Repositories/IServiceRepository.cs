﻿using System.Collections;
using System.Linq;

namespace MainApi.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Data.Entity;
    using Models;

    /// <summary>Интерфейс репозитория сервиса. </summary>
    public interface IServiceRepository
    {
        /// <summary>Получить все сервисы. </summary>
        /// <returns>Список сервисов. </returns>
        Task<IEnumerable<Service>> GetAll();


        /// <summary>Получить сервис. </summary>
        /// <param name="id">Идентификатор сервиса. </param>
        /// <returns>Сервис. </returns>
        Task<Service> Get(int id);

        /// <summary>Получить комментарии к сервису. </summary>
        /// <param name="id">Идентификатор сервиса. </param>
        /// <returns>Список комментариев. </returns>
        Task<IEnumerable<Comment>> GetCommentsById(int id);
    }

    /// <summary>Репозиторий сервисов. </summary>
    public class ServiceRepository : IServiceRepository
    {
        ModelMain db = new ModelMain();

        public async Task<IEnumerable<Service>> GetAll()
        {
            return await db.Service.ToListAsync();
        }

        public async Task<Service> Get(int id)
        {
            return await db.Service.FirstOrDefaultAsync(i => i.ID.Equals(id));
        }

        // TODO Возможно нужно сделать промежуточный уровень между контроллером и репозиторием, чтобы не городить в репозитории так
        public async Task<IEnumerable<Comment>> GetCommentsById(int id)
        {
            var entity = await db.Comment
                .Where(r => r.ServiceId == id)
                .Select(c => new
                {
                    c.ID,
                    c.CommentatorName,
                    c.Commentary,
                    c.ServiceId
                })
                .ToListAsync();

            var comments = entity
                .Select(my => new Comment
                {
                    ID = my.ID,
                    CommentatorName = my.CommentatorName,
                    Commentary = my.Commentary,
                    ServiceId = my.ServiceId
                })
                .ToList();

            return comments;
        }
    }
}