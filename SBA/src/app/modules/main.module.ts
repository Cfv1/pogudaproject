import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ServicesComponent} from '../components/services/services.component';
import {ServiceComponent} from '../components/service/service.component';
import {ServiceService} from '../repositories/service.service';
import {Service} from '../models/service';
import {RootService} from '../repositories/root.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ServicesComponent,
    ServiceComponent
  ],
  providers: [
    ServiceService,
    RootService,
    Service
  ]
})
export class MainModule { }
