import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {RootService} from './root.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient,
              private root: RootService) { }

  getAll() {
    return this.http.get(`${this.root.source}/service/getAll`);
  }

  getById(id: number) {
    return this.http.get(`${this.root.source}/service/get?id=${id}`);
  }
}
