import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../../repositories/service.service';
import {Service} from '../../models/service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  services: Service[] = [];
  constructor(private service: ServiceService) { }

  ngOnInit() {
    this.LoadServices();
  }

  LoadServices() {
    this.service.getAll()
      .subscribe((response: Service[]) => {
        this.services = response;
      }, (error) => {
        console.log(error);
      });
  }
}
